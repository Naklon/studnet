﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studnet
{
    class NewsView
    {
        public string Content { get; set; }
        public DateTime Date { get; set; }

        public NewsView(string con, DateTime date)
        {
            this.Content = con;
            this.Date = date;
        }
    }
}
