﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Text.RegularExpressions;

namespace Studnet
{
    /// <summary>
    /// Interaction logic for Edit.xaml
    /// </summary>
    public partial class Edit : Window
    {
        public Edit()
        {
            InitializeComponent();

            List<string> classesNames = new List<string>();

            foreach (DataRow item in App.rankTable.Rows)
            {
                classesNames.Add(item["name"].ToString());
            }
            listView.ItemsSource = classesNames;
            listView.SelectedIndex = 0;
        }

        public void FillEditWindow(StudnetDataSet.userRow user)
        {
            Application.Current.Properties["Editing"] = user.index_n;
            nameBox.Text = user.name;
            surnameBox.Text = user.surname;
            peselBox.Text = user.pesel;
            indexBox.Text = user.index_n;
            adressBox.Text = user.adress;
            emailBox.Text = user.mail;
            phoneBox.Text = user.phone;
            listView.SelectedIndex = user.rank_id-1;
        }

        private void SaveUser(object sender, RoutedEventArgs e)
        {
            string err = "";
            Regex mailReg = new Regex(@".+[@].+\.((com)|(pl)|(net))"), indexReg = new Regex(@"^[0-9]*$"), phoneReg = new Regex(@"^[0-9]*$"), peselReg = new Regex(@"^[0-9]*$");

            if (peselBox.Text.Length != 11 || !peselReg.Match(peselBox.Text).Success)
            {
                err += "PESEL\n";
            }

            if (indexBox.Text.Length != 5 || !indexReg.Match(indexBox.Text).Success)
            {
                err += "Nr indeksu\n";
            }

            if (phoneBox.Text.Length != 9 || !phoneReg.Match(phoneBox.Text).Success)
            {
                err += "Nr telefonu\n";
            }

            if (!mailReg.Match(emailBox.Text).Success)
            {
                err += "Adres email";
            }

            foreach (DataRow item in ((DataTable)App.userTable).Rows)
            {
                if (item["Nr indeksu"].ToString() == indexBox.Text && (string)Application.Current.Properties["Editing"] != indexBox.Text)
                {
                    err = "Podany user już istnieje.";
                    break;
                }
            }

            if (err.Length > 0)
            {
                MessageBox.Show("Poniższe dane są niepoprawne: " + "\n\n" + err);
            }
            else
            {
                App.userTable.Where(m => m.index_n == (string)Application.Current.Properties["Editing"]).Single().name = nameBox.Text;
                App.userTable.Where(m => m.index_n == (string)Application.Current.Properties["Editing"]).Single().surname = surnameBox.Text;
                App.userTable.Where(m => m.index_n == (string)Application.Current.Properties["Editing"]).Single().pesel = peselBox.Text;
                App.userTable.Where(m => m.index_n == (string)Application.Current.Properties["Editing"]).Single().adress = adressBox.Text;
                App.userTable.Where(m => m.index_n == (string)Application.Current.Properties["Editing"]).Single().mail = emailBox.Text;
                App.userTable.Where(m => m.index_n == (string)Application.Current.Properties["Editing"]).Single().phone = phoneBox.Text;
                App.userTable.Where(m => m.index_n == (string)Application.Current.Properties["Editing"]).Single().rank_id = listView.SelectedIndex + 1;
                App.userTable.Where(m => m.index_n == (string)Application.Current.Properties["Editing"]).Single().index_n = indexBox.Text;
                App.userAdapter.Update(App.userTable);
                this.Close();
            }
        }
    }
}
