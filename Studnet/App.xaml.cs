﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Studnet.StudnetDataSetTableAdapters;

namespace Studnet
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        public static StudnetDataSet ds = new StudnetDataSet();
        
        public static  StudnetDataSetTableAdapters.userTableAdapter userAdapter = new StudnetDataSetTableAdapters.userTableAdapter();
        public static StudnetDataSetTableAdapters.rankTableAdapter rankAdapter = new StudnetDataSetTableAdapters.rankTableAdapter();
        public static StudnetDataSetTableAdapters.groupTableAdapter groupAdapter = new StudnetDataSetTableAdapters.groupTableAdapter();
        public static StudnetDataSetTableAdapters.newsTableAdapter newsAdapter = new StudnetDataSetTableAdapters.newsTableAdapter();
        public static StudnetDataSetTableAdapters.calendar_entryTableAdapter calendar_entryAdapter = new StudnetDataSetTableAdapters.calendar_entryTableAdapter();

        public static StudnetDataSet.calendar_entryDataTable calendar_entryTable = new StudnetDataSet.calendar_entryDataTable();
        public static StudnetDataSet.userDataTable userTable = new StudnetDataSet.userDataTable();
        public static StudnetDataSet.rankDataTable rankTable = new StudnetDataSet.rankDataTable();
        public static StudnetDataSet.groupDataTable groupTable = new StudnetDataSet.groupDataTable();
        public static StudnetDataSet.ViewDataTable userViewTable = new StudnetDataSet.ViewDataTable();
        public static StudnetDataSetTableAdapters.ViewTableAdapter userViewAdapter = new StudnetDataSetTableAdapters.ViewTableAdapter();

        public static StudnetDataSet.newsDataTable newsTable = new StudnetDataSet.newsDataTable();
        public static void Initialize()
        {
            userTable = userAdapter.GetData();
            rankTable = rankAdapter.GetData();
            groupTable = groupAdapter.GetData();
            newsTable = newsAdapter.GetData();
            calendar_entryTable = calendar_entryAdapter.GetData();
            userViewTable = userViewAdapter.GetData();
        }

        public static string HashPassword(string pass)
        {
            string hashKey = "ohnn3w0vktrn40932ndfb", hashedPass = "";
            int pos = 0;

            for (int i = 0; i < pass.Length; i++)
            {
                hashedPass += pass.ElementAt(i).ToString() + hashKey[pos].ToString();
                pos++;
                if (pos >= hashKey.Length)
                {
                    pos = 0;
                }
            }

            return hashedPass;
        }
    }
}
