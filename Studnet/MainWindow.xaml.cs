﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;

namespace Studnet
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
            App.Initialize();
        }

        private void AddUser(object sender, RoutedEventArgs e)
        {
            UserAdd w1 = new UserAdd();
            w1.Show();
        }

        private void Login(object sender, RoutedEventArgs e)
        {
            bool ifLoginSucces = false;

            foreach (DataRow item in ((DataTable)App.userTable).Rows)
            {
                if(item["index_n"].ToString() == indexBox.Text)
                {
                    if(item["password"].ToString()==passBox.Password)
                    {
                        Application.Current.Properties["Username"] = item["index_n"].ToString();
                        Application.Current.Properties["Permissions"] = item["rank_id"].ToString();
                        ifLoginSucces = true;
                    }
                    break;
                }
            }

            if(!ifLoginSucces)
            {
                MessageBox.Show("Niepoprawny login i/lub hasło.");
            }
            else
            {
                PermissionLogin();
            }
        }

        public void PermissionLogin()
        {
            Window w = new Window();

            if (Convert.ToInt32(App.userTable.Where(m => m.index_n == Application.Current.Properties["Username"].ToString()).Single().if_pass_changed) == 0)
            {
                PasswordChange w1 = new PasswordChange();
                w1.Show();
                this.Close();
                return;
            }
            
            switch (Application.Current.Properties["Permissions"].ToString())
            {
                case "1":
                    w = new User();
                    break;
                case "2":
                    w = new User();
                    break;
                case "3":
                    w = new AdminPanel();
                    break;
            }

            w.Show();
            this.Close();
        }
    }
}
