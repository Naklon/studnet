﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studnet
{
    class CalendarView
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public DateTime To { get; set; }
        public string Description { get; set; }

        public CalendarView(string name, DateTime date,int duration,string desc)
        {
            this.Name = name;
            this.Date = date;
            this.To = this.Date.AddDays(duration);
            this.Description = desc;
        }
    }
}
