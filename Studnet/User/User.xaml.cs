﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Studnet
{
    /// <summary>
    /// Interaction logic for User.xaml
    /// </summary>
    public partial class User : Window
    {
        protected override void OnClosed(EventArgs e)
        {
  
        }

        public User()
        {
            InitializeComponent();

            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);

            string name = App.userTable.Where(m => m.index_n == Application.Current.Properties["Username"].ToString()).Single().name + " " + App.userTable.Where(m => m.index_n == Application.Current.Properties["Username"].ToString()).Single().surname;

            string per = App.rankTable.Where(m => m.id == App.userTable.Where(o => o.index_n == Application.Current.Properties["Username"].ToString()).Single().rank_id).Single().name;

            usrTxt.Text = name;
            perTxt.Text = per;

            List<NewsView> userNews = new List<NewsView>();

            foreach (var item in App.newsTable)
            {
                if(item.group_id == App.userTable.Where(l => l.index_n == Application.Current.Properties["Username"].ToString()).Single().group_id)
                {
                    userNews.Add(new NewsView(item.content_text, item.date));
                }
            }

            this.newsGrid.ItemsSource = userNews;

            List<CalendarView> userCal = new List<CalendarView>();

            foreach (var item in App.calendar_entryTable)
            {
                if(item.group_id == App.userTable.Where(l => l.index_n == Application.Current.Properties["Username"].ToString()).Single().group_id)
                {
                    userCal.Add(new CalendarView(item.name, item.date, item.duration, item.description));
                }
            }
            this.calGrid.ItemsSource = userCal;
        }

        private void Logout(object sender, RoutedEventArgs e)
        {
            Application.Current.Properties["Username"] = "";
            MainWindow w = new MainWindow();
            w.Show();
            this.Close();
        }

        private void ExitProgram(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
