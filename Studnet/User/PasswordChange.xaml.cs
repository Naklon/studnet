﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Studnet
{
    /// <summary>
    /// Interaction logic for PasswordChange.xaml
    /// </summary>
    public partial class PasswordChange : Window
    {
        public PasswordChange()
        {
            InitializeComponent();

            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }

        private void SavePassword(object sender, RoutedEventArgs e)
        {
            if(passBox.Password != passConfBox.Password)
            {
                MessageBox.Show("Podane hasła się nie zgadzają.");
            }
            else
            {

                App.userTable.Where(m => m.index_n == Application.Current.Properties["Username"].ToString()).Single().password = passBox.Password;
                App.userTable.Where(m => m.index_n == Application.Current.Properties["Username"].ToString()).Single().if_pass_changed = 1;
                App.userAdapter.Update(App.userTable);

                Window w = new Window();

                switch (Application.Current.Properties["Permissions"].ToString())
                {
                    case "1":
                        w = new User();
                        break;
                    case "2":
                        w = new User();
                        break;
                    case "3":
                        w = new AdminPanel();
                        break;
                }
                w.Show();
                this.Close();
            }
        }
    }
}
