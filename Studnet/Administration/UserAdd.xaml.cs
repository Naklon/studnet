﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Text.RegularExpressions;

namespace Studnet
{
    /// <summary>
    /// Interaction logic for UserAdd.xaml
    /// </summary>
    public partial class UserAdd : Window
    {
        public UserAdd()
        {
            InitializeComponent();
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);

            List<string> classesNames = new List<string>();

            foreach (DataRow item in App.rankTable.Rows)
            {
                classesNames.Add(item["name"].ToString());
            }
            listView.ItemsSource = classesNames;
        }

        private void SaveUser(object sender, RoutedEventArgs e)
        {
            DataRow dr = App.ds.user.NewRow();

            nameBox.Text = Regex.Replace(nameBox.Text, @"\s+", "");
            surnameBox.Text = Regex.Replace(surnameBox.Text, @"\s+", "");

            string err = "";

            dr["name"] = nameBox.Text;
            dr["surname"] = surnameBox.Text;
            dr["pesel"] = peselBox.Text;
            dr["index_n"] = indexBox.Text;
            dr["adress"] = adressBox.Text;
            dr["mail"] = emailBox.Text;
            dr["phone"] = phoneBox.Text;
            dr["rank_id"] = Convert.ToInt32(listView.SelectedIndex)+1;
            dr["if_pass_changed"] = 0;

            Regex mailReg = new Regex(@".+[@].+\.((com)|(pl)|(net))"), indexReg = new Regex(@"^[0-9]*$"), phoneReg = new Regex(@"^[0-9]*$"), peselReg = new Regex(@"^[0-9]*$");            

            if (peselBox.Text.Length != 11 || !peselReg.Match(peselBox.Text).Success)
            {
                err += "PESEL\n";
            }

            if (indexBox.Text.Length != 5 || !indexReg.Match(indexBox.Text).Success)
            {
                err += "Nr indeksu\n";
            }

            if (phoneBox.Text.Length != 9 || !phoneReg.Match(phoneBox.Text).Success)
            {
                err += "Nr telefonu\n";
            }

            if(!mailReg.Match(emailBox.Text).Success)
            {
                err += "Adres email";
            }

            foreach (DataRow item in ((DataTable)App.userTable).Rows)
            {
                if (item["index_n"].ToString() == indexBox.Text)
                {
                    err = "Podany user już istnieje.";
                    break;
                }
            }

            if (err.Length > 0)
            {
                MessageBox.Show("Poniższe dane są niepoprawne: " + "\n\n" + err);
            }
            else
            {
                string pass = System.IO.Path.GetRandomFileName();
                pass = pass.Replace(".", "");

                MailMessage mail = new MailMessage("studnet.system@gmail.com", emailBox.Text);

                mail.Subject = "Nowe konto w systemie Studnet";
                mail.Body = "Witaj, " + nameBox.Text + " " + surnameBox.Text + "\nZostało stworzone dla Ciebie nowe konto w systemie Studnet. Twoje tymczasowe hasło to\n" + pass + "\nZmień je od razu po zalogowaniu.\nPozdrawiamy,\nAdministracja systemu Studnet.";
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential("studnet.system@gmail.com", "LubiePlacki123"),
                    Timeout = 20000
                };
                
                try {
                    smtp.Send(mail);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Wystąpił błąd podczas wysyłania wiadomości email. Spróbuj ponownie.");
                }

                dr["password"] = pass;
                App.ds.user.Rows.Add(dr);
                App.userAdapter.Update(App.ds.user);
                this.Close();
            }
        }
    }
}
