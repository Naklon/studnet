﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;

namespace Studnet
{
    /// <summary>
    /// Interaction logic for ManageUsers.xaml
    /// </summary>
    public partial class ManageUsers : Window
    {
        public ManageUsers()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            Studnet.StudnetDataSet StudnetDataSet = ((Studnet.StudnetDataSet)(this.FindResource("StudnetDataSet")));
            // Load data into the table View. You can modify this code as needed.
            Studnet.StudnetDataSetTableAdapters.ViewTableAdapter StudnetDataSetViewTableAdapter = new Studnet.StudnetDataSetTableAdapters.ViewTableAdapter();
            StudnetDataSetViewTableAdapter.Fill(StudnetDataSet.View);
            System.Windows.Data.CollectionViewSource viewViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("viewViewSource")));
            viewViewSource.View.MoveCurrentToFirst();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                this.RemoveUser(sender, e);
                e.Handled = true;
            }
        }

        private void RemoveUser(object sender, RoutedEventArgs e)
        {
            int count = dataGrid.SelectedItems.Count;
            bool ifDelete = true;

            if (dataGrid.SelectedItem.ToString() != "{NewItemPlaceholder}")
            {
                if(MessageBox.Show("Z bazy zostanie usunięta następująca liczba userów: " + count.ToString() + ". Czy chcesz kontynuować?", "Potwierdzenie",MessageBoxButton.YesNo,MessageBoxImage.Warning) == MessageBoxResult.No)
                {
                    ifDelete = false;
                }
            }

            if (ifDelete && count > 0)
            {
                App.userTable = App.userAdapter.GetData();
                foreach (var item in dataGrid.SelectedItems)
                {
                    if (dataGrid.SelectedItem.ToString() != "{NewItemPlaceholder}")
                    {
                        App.userTable.ElementAt(dataGrid.Items.IndexOf(item)).Delete();
                        
                    }
                }
                App.userAdapter.Update(App.userTable);
                App.userViewTable = App.userViewAdapter.GetData();
                dataGrid.ItemsSource = App.userViewTable;
            }
        }

        private void EditUser(object sender, RoutedEventArgs e)
        {
            App.userTable = App.userAdapter.GetData();
            if (dataGrid.SelectedItem.ToString() != "{NewItemPlaceholder}")
            {
                Edit w = new Edit();
                w.FillEditWindow(App.userTable.ElementAt(dataGrid.SelectedIndex));
                w.ShowDialog();
                App.userViewTable = App.userViewAdapter.GetData();
                dataGrid.ItemsSource = App.userViewTable;
            }
        }
    }
}
