﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Studnet.Administration
{
    /// <summary>
    /// Interaction logic for NewsAdd.xaml
    /// </summary>
    public partial class NewsAdd : Window
    {
        public NewsAdd()
        {
            InitializeComponent();
            StudnetDataSet dataSet = new StudnetDataSet();
            StudnetDataSetTableAdapters.groupTableAdapter GroupTableAdapter = new StudnetDataSetTableAdapters.groupTableAdapter();
            
            DataTable dt = new DataTable();
            dt = GroupTableAdapter.GetData();
            foreach (DataRow VARIABLE in dt.Rows)
            {
                comboBox.Items.Add(VARIABLE["name"].ToString());
            }
            //comboBox.ItemsSource = GroupTableAdapter.Tables
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            DataRow dr = App.ds.news.NewRow();
            dr["date"] = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day;
            dr["content_text"] = textBox.Text;
            //szukanie id usera

            StudnetDataSet dataSet = new StudnetDataSet();
            StudnetDataSetTableAdapters.userTableAdapter UserTableAdapter = new StudnetDataSetTableAdapters.userTableAdapter();

            DataTable dt = new DataTable();
            dt = UserTableAdapter.GetData();
            string uid = "";
            foreach (DataRow VARIABLE in dt.Rows)
            {
                if (VARIABLE["index_n"].ToString().Equals(Application.Current.Properties["Username"].ToString()))
                    uid = VARIABLE["id"].ToString();
            }

            dr["user_id"] = uid;
            dr["group_id"] = App.groupTable.Where(m=>m.name == comboBox.SelectedItem.ToString()).Single().id;
            App.ds.news.Rows.Add(dr);
            App.newsAdapter.Update(App.ds.news);
            this.Close();
        }
    }
}
