﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Studnet.Administration
{
    /// <summary>
    /// Interaction logic for CalendarAdd.xaml
    /// </summary>
    public partial class CalendarAdd : Window
    {
        public CalendarAdd()
        {
            InitializeComponent();
            DataTable dt = new DataTable();
            dt = App.groupAdapter.GetData();
            foreach (DataRow VARIABLE in dt.Rows)
            {
                groupComboBox.Items.Add(VARIABLE["id"].ToString());
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            DataRow dr = App.ds.calendar_entry.NewRow();
            dr["date"] = startTimeTextBox.Text;
            dr["duration"] = timeTextBox.Text;
            dr["name"] = descriptionTextBox.Text;
            dr["description"] = contenstTextBox.Text;
            string uid = "";

            DataTable dt = App.userAdapter.GetData();
            foreach (DataRow VARIABLE in dt.Rows)
            {
                if (VARIABLE["index_n"].ToString().Equals(Application.Current.Properties["Username"].ToString()))
                    uid = VARIABLE["id"].ToString();
            }
            dr["user_id"] = uid;
            dr["group_id"] = groupComboBox.SelectedItem.ToString();

            App.ds.calendar_entry.Rows.Add(dr);
            App.calendar_entryAdapter.Update(App.ds.calendar_entry);
            this.Close();
        }
    }
}
