﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Studnet.Administration;

namespace Studnet
{
    /// <summary>
    /// Interaction logic for AdminPanel.xaml
    /// </summary>
    public partial class AdminPanel : Window
    {
        protected override void OnClosed(EventArgs e)
        {
            if ((string)Application.Current.Properties["Username"] != "")
            {
                base.OnClosed(e);

                Application.Current.Shutdown();
            }
        }

        public AdminPanel()
        {
            InitializeComponent();

            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);

            string name = Enumerable.Where(App.userTable, m => m.index_n == Application.Current.Properties["Username"].ToString()).Single().name +" " + Enumerable.Where(App.userTable, m => m.index_n == Application.Current.Properties["Username"].ToString()).Single().surname;

            string per = Enumerable.Where(App.rankTable, m => m.id == Enumerable.Where(App.userTable, o => o.index_n == Application.Current.Properties["Username"].ToString()).Single().rank_id).Single().name;

            usrTxt.Text = name;
            perTxt.Text = per;
            dataGrid.ItemsSource = App.newsAdapter.GetData();

        }

        private void AddUser(object sender, RoutedEventArgs e)
        {
            UserAdd w = new UserAdd();
            w.Show();
        }

        private void ShowUsers(object sender, RoutedEventArgs e)
        {
            ManageUsers w = new ManageUsers();
            w.Show();
        }

        private void ExitProgram(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void logoutBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Properties["Username"] = "";
            MainWindow w = new MainWindow();
            w.Show();
            this.Close();
        }

        private void addNewsButton_Click(object sender, RoutedEventArgs e)
        {

                NewsAdd w = new NewsAdd();
                w.ShowDialog();
        }

        private void removeNewsButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid.SelectedItems.Count == 0)
            {
                MessageBox.Show("Nalzey wybrac jakis news");
            }
            else
            {
                int elemNumber;
                int cnt;
                foreach (var VARIABLE in dataGrid.SelectedItems)
                {
                    elemNumber = dataGrid.Items.IndexOf(VARIABLE);
                    cnt = App.newsTable.Count;
                    App.newsTable.ElementAt(dataGrid.Items.IndexOf(VARIABLE)).Delete(); // tutaj sypie bledem!!!!!!!!
                }
                App.newsAdapter.Update(App.newsTable);
                App.newsTable = App.newsAdapter.GetData();
                dataGrid.ItemsSource = App.newsTable;
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            NewsEdit w = new NewsEdit();
            w.ShowDialog();
        }

        private void dataGrid1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TabItem_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            dataGrid1.ItemsSource = App.calendar_entryAdapter.GetData();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            CalendarAdd w = new CalendarAdd();
            w.Show();
        }
    }
}
